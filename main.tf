# Configure the AWS Provider
provider "aws" {
  region = "ap-south-1"
  shared_config_files      = ["/Users/Tushar.Patil/.aws/config"]
  shared_credentials_files = ["/Users/Tushar.Patil/.aws/credentials"]
  
}



resource "aws_iam_role" "lambda_role_rest" {
  name = "terraform_aws_lambda_role_rest"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "iam_policy_for_lambda_rest" {
  name        = "aws_iam_policy_for_terraform_aws_lambda_role_rest"
  path        ="/"
  description = "Aws Iam Policy for Managing aws lambda role"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [ 
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:CreateLogEvents"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:logs:*:*:*"
    },

    {
            "Effect": "Allow",
            "Action": [
                "execute-api:Invoke",
                "execute-api:ManageConnections"
            ],
            "Resource": "arn:aws:execute-api:*:*:*"
        }
  ]
}
EOF
}




resource "aws_iam_role_policy_attachment" "attach-iam-policy-to-iam-role_rest" {
  role       = aws_iam_role.lambda_role_rest.name
  policy_arn = aws_iam_policy.iam_policy_for_lambda_rest.arn
}


data "archive_file" "zip_the_nodejs_code" {
  type        = "zip"
  source_dir = "${path.module}/node/"
  output_path = "${path.module}/node/index.zip"
}


resource "aws_lambda_function" "terraform_lambda_func_rest" {
  filename      = "${path.module}/node/index.zip"
  function_name = "terraform_lambda_Rest_function"
  role          = aws_iam_role.lambda_role_rest.arn
  handler       = "index.handler"
  runtime       = "nodejs16.x"
  timeout       = 10
  depends_on = [aws_iam_role_policy_attachment.attach-iam-policy-to-iam-role_rest]
}

resource "aws_lambda_permission" "allow_api" {
  statement_id  = "AllowAPIgatewayInvokation"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.terraform_lambda_func_rest.function_name
  principal     = "apigateway.amazonaws.com"
}

################ API GATEWAY Start ################

resource "aws_api_gateway_rest_api" "terraform-nodejs-rest-api-gateway" {
  name = "terraform-nodejs-rest-api-gateway"
  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

resource "aws_api_gateway_resource" "user" {
  rest_api_id = aws_api_gateway_rest_api.terraform-nodejs-rest-api-gateway.id
  parent_id   = aws_api_gateway_rest_api.terraform-nodejs-rest-api-gateway.root_resource_id
  path_part   = "{proxy+}"
}





// ALL
resource "aws_api_gateway_method" "All" {
  rest_api_id       = aws_api_gateway_rest_api.terraform-nodejs-rest-api-gateway.id
  resource_id       = aws_api_gateway_resource.user.id
  http_method       = "ANY"
  authorization     = "NONE"
  api_key_required  = false
  request_parameters = {"method.request.path.proxy" = true}
  
}


resource "aws_api_gateway_integration" "integration-get" {
  rest_api_id             = aws_api_gateway_rest_api.terraform-nodejs-rest-api-gateway.id
  resource_id             = aws_api_gateway_resource.user.id
  http_method             = aws_api_gateway_method.All.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.terraform_lambda_func_rest.invoke_arn
}

resource "aws_api_gateway_deployment" "deployment1" {
  rest_api_id = aws_api_gateway_rest_api.terraform-nodejs-rest-api-gateway.id

  

  depends_on = [aws_api_gateway_integration.integration-get]
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_stage" "example" {
  deployment_id = aws_api_gateway_deployment.deployment1.id
  rest_api_id   = aws_api_gateway_rest_api.terraform-nodejs-rest-api-gateway.id
  stage_name    = "dev"
}


resource "aws_db_instance" "nodedb" {
  allocated_storage    = 50
  #db_instance_identifier="mydb"
  identifier           ="mydb"
  db_name              = "mydb"
  engine               = "mysql"
  engine_version       = "8.0.28"
  instance_class       = "db.t3.micro"
  username             = "root"
  password             = "root1234"
  parameter_group_name = "default.mysql8.0"
  skip_final_snapshot  = true
}

variable "rds_vpc_id" {
  default = "vpc-09d9c551f0cb3a95d"
  description = "Our default RDS virtual private cloud (rds_vpc)."
}

resource "aws_security_group" "mydb" {
  name = "mydb"

  description = "RDS mysql servers (terraform-managed)"
  vpc_id = "${var.rds_vpc_id}"

  # Only mysql in
  ingress {
    from_port = 0
    to_port = 3306
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  
}

output "complete_unvoke_url" {
    value = "${aws_api_gateway_deployment.deployment1.invoke_url}${aws_api_gateway_stage.example.stage_name}/user"
    }


output "terraform_aws_role_output"{
    value = aws_iam_role.lambda_role_rest.name
}

output "terraform_aws_role_arn_output"{
    value= aws_iam_role.lambda_role_rest.arn
}

output "database_endpoint"{
    value=aws_db_instance.nodedb.endpoint
}

output "database_user"{
    value=aws_db_instance.nodedb.username
}

output "database_password"{
    value=aws_db_instance.nodedb.password 
    sensitive = true 
}

output "database_vpc"{
    value =aws_db_instance.nodedb.vpc_security_group_ids
}